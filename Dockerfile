FROM php:5.6-apache

# Install helper binaries
RUN apt-get update && apt-get install -y wget git unzip php5-cli libicu-dev libmcrypt-dev libpng-dev zlib1g-dev

RUN docker-php-ext-install mysql mysqli pdo pdo_mysql intl mcrypt gd mbstring zip

# Install composer
RUN wget https://getcomposer.org/installer
RUN php installer
RUN mv composer.phar /usr/local/bin/composer
RUN /usr/local/bin/composer self-update

# Require the composser asset plugin
RUN composer global require "fxp/composer-asset-plugin:^1.2.0"

# Enable Mod-Rewrite
RUN a2enmod rewrite

# Expose Port 80 of apache
EXPOSE 80

# Configure the yii2 application
COPY ./conf/vhost.conf /etc/apache2/sites-available/000-default.conf

VOLUME ["/var/www/yii2", "/var/log/apache2", "/etc/apache2"]
ENTRYPOINT ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]

# Set the working dirfor running additional
WORKDIR "/var/www/yii2"